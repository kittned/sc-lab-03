import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextArea;


public class Veiw extends JFrame {
	
	public Veiw() {
		   createFrame();
		   
	   }
	   public void createFrame() {
		   showProjectName = new JLabel("SC Lab 03");
		   
		   showResults = new JTextArea("your resutls will be showed here");
		   showResults.setPreferredSize(new Dimension(390,280));
		   
		   getInputs = new JTextArea();
		   getInputs.setPreferredSize(new Dimension(380,300));
		   
		   submitButton = new JButton("Submit!");
		   substringB = new JRadioButton("Substring");
		   ngramB = new JRadioButton("N-Gram");
		   
		   rightPanel = new JPanel();
		   rightPanel.setPreferredSize(new Dimension(400,300));
		   setLayout(new BorderLayout());
		   
		   add(showProjectName, BorderLayout.NORTH);
		   add(getInputs, BorderLayout.WEST);
		   add(rightPanel, BorderLayout.EAST);
		   
		   rightPanel.add(showResults, BorderLayout.CENTER);
		   rightPanel.add(substringB, BorderLayout.SOUTH);
		   rightPanel.add(ngramB, BorderLayout.SOUTH);
		   rightPanel.add(submitButton, BorderLayout.SOUTH);
		   
		   
	   }
	   public ArrayList<String> getInput()
	   {
		  ArrayList<String> sentences = new ArrayList<String>();
		  StringTokenizer getSentences = new StringTokenizer(getInputs.getText(), "\n");
		  
		  while (getSentences.hasMoreTokens())
		  {
			  sentences.add(getSentences.nextToken());
		  }
		  
		  return sentences;
	   }
	   
	   public void checkButton(model start)
	   {
		   if (substringB.isSelected())
		   {
			   extendResult(start.tokenizer());
		   }
		   
		   if (ngramB.isSelected())
		   {
			   extendResult(start.n_gram());
		    	
		   }
		   
		   extendResult(start.toASCII());
		   extendResult(start.modASCII());
	   }
	   
	   public void setProjectName(String s) {
		   showProjectName.setText(s);
	   }
	   
	   public void setResult(String str) {
	       this.str = str;
		   showResults.setText(str);
	   }
	   
	   public void extendResult(String str) {
		   this.str = this.str+str+"\n";
		   showResults.setText(this.str);
	   }
	   
	   public void setListener(ActionListener list) {
		   submitButton.addActionListener(list);
	   }

	   private JLabel showProjectName;
	   
	   private JTextArea showResults;
	   private JTextArea getInputs;
	   
	   private JButton submitButton;
	   private JRadioButton substringB;
	   private JRadioButton ngramB;
	   
	   private String str;
	   
	   private JPanel rightPanel;

}
