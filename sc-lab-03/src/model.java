import java.util.ArrayList;
import java.util.StringTokenizer;

import javax.swing.JOptionPane;

public class model {
	
	private String input;
	private String noSpace = "";
	private String getN;
	private String getMod;
	
	private byte[] ascii;
	private int n;
	private int sumASCII = 0;
	
	private StringTokenizer tokenizer;
	
	private ArrayList<String> tokens = new ArrayList<String>();
	private ArrayList<String> nGram = new ArrayList<String>();
	
	public model(String getInput)
	{
		input = getInput;
		
		tokenizer = new StringTokenizer(input);
		
		while (tokenizer.hasMoreTokens())
		{
			tokens.add(tokenizer.nextToken());
		}	
		
		
		for (int i = 0; i < tokens.size();i++)
		{
			noSpace = noSpace + tokens.get(i);
			
		}
		
	}
	
	public String tokenizer()
	{
		return Integer.toString(tokens.size()) + " tokens\n" + tokens;
	}
	
	public String n_gram()
	{
		getN = JOptionPane.showInputDialog("Enter a number for n-geram : ");

		n =  Integer.parseInt(getN);
		
		for(int i = 0; n+i < noSpace.length();i++)
		{
			nGram.add(noSpace.substring(0+i, n+i));
		}
		
		return Integer.toString(nGram.size()) + " N-Grams\n" + nGram;
	}
	
	public String toASCII()
	{
		ascii = noSpace.getBytes();
		for (int i = 0; i < ascii.length ;i++)
		{
			sumASCII = sumASCII + ascii[i];
		}
		
		return "Summary of ASCII in the String : " + Integer.toString(sumASCII);
	}
	
	public String modASCII()
	{
		getMod = JOptionPane.showInputDialog("Enter a Modulus number : ");
		int mod = sumASCII % Integer.parseInt(getMod);
		return Integer.toString(sumASCII) + " % " + getMod + " = " + Integer.toString(mod) + "\n";
	}

}
